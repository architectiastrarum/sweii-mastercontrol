const mongoose = require('mongoose');
const User = require('./models/User');

const getUserByEmail = async function (email) {
    return await User.findOne({email: email}).lean().exec();
};

const updateUser = async function (user) {
    const doc = await User.findOneAndUpdate({email: user.email}, user, {upsert: true, new: true}).exec();
    return doc._id;
};

const dropUser = async function (user) {
    return await User.deleteOne(user).exec();
};

module.exports.getUserByEmail = getUserByEmail;
module.exports.updateUser = updateUser;
module.exports.dropUser = dropUser;
