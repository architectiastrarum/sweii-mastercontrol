'use strict';
const util = require('util');
const crypto = require('crypto');
const fs = require('fs');
const exec = util.promisify(require('child_process').exec);
//let's just assume linux to make our lives easier
const generateKeypair = async function (uuid, passphrase) {
    const cmd = `yes y | ssh-keygen -t ed25519 -N '${passphrase}' -f '${uuid}' -C ${uuid}-SWEII-MC`;
    const {stdout, stderr} = await exec(cmd);
    return !stderr;
};

const getPrivateKey = async function (uuid) {
    const cmd = `cat ${uuid}`;
    const {stdout, stderr} = await exec(cmd);
    fs.unlinkSync(uuid);
    return stdout;
};

const getPublicKey = async function (uuid) {
    const cmd = `cat ${uuid}.pub`;
    const {stdout, stderr} = await exec(cmd);
    return stdout.trim();
};

const encryptPassword = async function (plainText) {
    const salt = await genRandomString();
    return await sha512(plainText, salt);
};

const genRandomString = async function (length = 256) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
};
const genRandomStringSync = function (length = 256) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
};

const sha512 = async function (password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    /** Hashing algorithm sha512 */
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

const verifyPassword = async function (password, hash, salt) {
    const newHash = (await sha512(password, salt)).passwordHash;
    return hash === newHash;
};

const generateChecksum = async (str, algorithm, encoding) => {
    return crypto
        .createHash(algorithm || 'md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex');
};

module.exports.generateKeypair = generateKeypair;
module.exports.getPrivateKey = getPrivateKey;
module.exports.getPublicKey = getPublicKey;
module.exports.encryptPassword = encryptPassword;
module.exports.genRandomString = genRandomString;
module.exports.genRandomStringSync = genRandomStringSync;
module.exports.verifyPassword = verifyPassword;
module.exports.generateChecksum = generateChecksum;