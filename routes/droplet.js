const express = require('express');
const router = express.Router();
const validator = require('validator');

const DigitalOcean = require('../DigitalOcean');
const UserManager = require('../UserManager');

router.get('/dropletsForUser', function (req, res) {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }

    //get user so we can access children droplet schema
    UserManager.getUserByEmail(req.user.email).then((user) => {
        res.json(user.droplets);
        return true;
    });


});

router.get('/', async (req, res) => {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    //check for expected parameters
    if (!req.body.dropletId) {
        return res.status(422).json({error: 'dropletId not set!'});
    }
    if (!validator.isNumeric(req.body.dropletId)) {
        return res.status(422).json({error: 'dropletId was not numeric!'});
    }
    //ensure authorization
    const dropletsForUser = (await UserManager.getUserByEmail(req.user.email)).droplets;
    const requestedDropletId = parseInt(req.body.dropletId);
    const dropletIds = dropletsForUser.map((droplet) => {
        return droplet.dropletId;
    });
    if (!dropletIds.includes(requestedDropletId)) {
        //user is not authorized to check that droplet
        return res.status(403).json({error: 'You are not authorized to get this droplet!'});
    }
    return res.json(await DigitalOcean.getMachine(requestedDropletId));
});

router.post('/createDroplet', async (req, res) => {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    //check for expected parameters
    if (!req.body.team) {
        return res.status(422).json({error: 'team not set!'});
    }
    if (!validator.isAlphanumeric(req.body.team)) {
        return res.status(422).json({error: 'team was not alphanumeric!'});
    }
    if (!req.body.env) {
        return res.status(422).json({error: 'env not set!'});
    }
    if (!validator.isAlphanumeric(req.body.env)) {
        return res.status(422).json({error: 'env was not alphanumeric!'});
    }
    const user = await UserManager.getUserByEmail(req.user.email);
    const subDomain = req.body.team + '.' + req.body.env;
    const email = user.email;
    const fingerprint = user.keyFingerprint;
    if (!fingerprint) {
        //user has not generated keypair
        return res.status(400).json({
            error: 'keypair has not been generated for this account!'
        });
    }
    const dropletId = await DigitalOcean.createMachine(subDomain, fingerprint);
    if (!dropletId) return res.status(503).json({error: 'Droplet creation failed!'});

    res.status(202).json({dropletId: dropletId});

    await UserManager.updateUser({
        email: email, $push: {
            droplets: {
                dropletId: dropletId,
                subDomain: subDomain
            }
        }
    });

    const ipsP = new Promise((resolve, reject) => {
        (function waitForUnlock() {
            DigitalOcean.getMachine(dropletId).then(machine => {
                if (machine.droplet.locked) {
                    setTimeout(waitForUnlock, 1000);
                }
                else {
                    const ipv4 = machine.droplet.networks.v4.filter(d => d.type === 'public')[0].ip_address;
                    const ipv6 = machine.droplet.networks.v6.filter(d => d.type === 'public')[0].ip_address;
                    const priv = machine.droplet.networks.v4.filter(d => d.type === 'private')[0].ip_address;
                    resolve({
                        ipv4: ipv4,
                        ipv6: ipv6,
                        private: priv
                    });
                }
            });
        })();
    });
    return ipsP.then(ips => {
        DigitalOcean.createDNSRecord(subDomain, ips.ipv4, ips.ipv6).then(ids => {
            if (ids) {
                const User = require('../models/User');
                return User.findOneAndUpdate({
                        "email": email, 'droplets.dropletId': dropletId
                    },
                    {
                        "$set": {
                            'droplets.$.ipv4': ips.ipv4,
                            'droplets.$.ipv6': ips.ipv6,
                            'droplets.$.private': ips.private,
                            'droplets.$.dnsIds': ids
                        }
                    }, {upsert: true, new: true}).exec();
            } else {
                console.error('DNS record creation failed!');
                return false;
            }
        });
    });
});

router.get('/status', async (req, res) => {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    //check for expected parameters
    if (!req.body.dropletId) {
        return res.status(422).json({error: 'team not set!'});
    }
    if (!validator.isNumeric(req.body.dropletId)) {
        return res.status(422).json({error: 'team was not numeric!'});
    }
    //ensure authorization
    const dropletsForUser = (await UserManager.getUserByEmail(req.user.email)).droplets;
    const requestedDropletId = parseInt(req.body.dropletId);
    const dropletIds = dropletsForUser.map((droplet) => {
        return droplet.dropletId;
    });
    if (!dropletIds.includes(requestedDropletId)) {
        //user is not authorized to check that droplet
        return res.status(403).json({error: 'You are not authorized to get the status of the droplet!'});
    }
    const droplet = await DigitalOcean.getMachine(requestedDropletId);
    return res.json(droplet);

});

router.delete('/', async (req, res) => {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    //check for expected parameters
    if (!req.body.dropletId) {
        return res.status(422).json({error: 'team not set!'});
    }
    if (!validator.isNumeric(req.body.dropletId)) {
        return res.status(422).json({error: 'team was not numeric!'});
    }
    //ensure authorization
    const dropletsForUser = (await UserManager.getUserByEmail(req.user.email)).droplets;
    const requestedDropletId = parseInt(req.body.dropletId);
    const dropletIds = dropletsForUser.map((droplet) => {
        return droplet.dropletId;
    });
    if (!dropletIds.includes(requestedDropletId)) {
        //user is not authorized to check that droplet
        return res.status(403).json({error: 'You are not authorized to delete this droplet!'});
    }
    res.status(202).send();
    const User = require('../models/User');
    const deleteFromMongoP = User.updateOne({
        email: req.user.email
    }, {
        $pull: {"droplets": {dropletId: requestedDropletId}}
    }).exec();
    const deleteFromDOP = DigitalOcean.deleteMachine(requestedDropletId);
    const dropletDnsIds = dropletsForUser.find((d) => d.dropletId === requestedDropletId).dnsIds;
    const deleteDNSRecordsP = DigitalOcean.deleteDNSRecords(dropletDnsIds);
    if (!await deleteFromMongoP || !await deleteFromDOP || !await deleteDNSRecordsP) {
        console.error(`Could not delete droplet ${requestedDropletId}!`);
        return false;
    }
    return true;
});

module.exports = router;
