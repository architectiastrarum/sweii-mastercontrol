const express = require('express');
const router = express.Router();
const pjson = require('../package.json');
const moment = require('moment');
/* GET home page. */
router.get('/', function (req, res, next) {
    res.json({
        status: 'OK',
        message: `${pjson.name} Online.`,
        version: pjson.version,
        uptime: moment().startOf('day')
            .seconds(process.uptime())
            .format('H:mm:ss')
    });
});

module.exports = router;
