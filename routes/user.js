const express = require('express');
const router = express.Router();
const validator = require('validator');

const User = require('../models/User');
const UserManager = require('../UserManager');
const Security = require('../security');
const DigitalOcean = require('../DigitalOcean');

router.get('/', function (req, res) {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    UserManager.getUserByEmail(req.user.email).then((user) => {
        if (!!user.droplets) {
            user.droplets = user.droplets.map((d) => d.subDomain);
        }
        return res.json(user);
    })
});

router.get('/keypair', async (req, res) => {
    //ensure authentication
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    const user = await UserManager.getUserByEmail(req.user.email);
    if (!!user.publicKey) {
        //user has already generated keypair, return public key and fingerprint
        return res.json({
            publicKey: user.publicKey,
            keyFingerprint: user.keyFingerprint
        });
    } else {
        //user has not generated a keypair
        res.status(400).json({
            message: 'keypair has not been generated!'
        });
    }
});

router.post('/keypair', async (req, res) => {
    if (!req.user) {
        return res.status(401).redirect('/api/auth/google');
    }
    const user = await UserManager.getUserByEmail(req.user.email);
    if (!!user.publicKey) {
        //user has already generated keypair, return public key and fingerprint
        return res.json({
            publicKey: user.publicKey,
            keyFingerprint: user.keyFingerprint
        });
    }
    //check for expected parameters
    if (!req.body.passphrase) {
        return res.status(422).json({error: 'passphrase not set!'});
    }
    const passphrase = req.body.passphrase;
    const uuid = user.email.substr(0, user.email.indexOf('@'));
    const generated = await Security.generateKeypair(uuid, passphrase);
    if (!generated) res.status(500).json({error: 'Could not generate keypair for unknown reason! Contact sysadmin.'});
    const publicKey = await Security.getPublicKey(uuid);
    UserManager.updateUser({
        email: user.email,
        publicKey: publicKey
    });
    //then get fingerprint from DO
    const fingerprint = await DigitalOcean.addPublicKey(uuid, publicKey);
    UserManager.updateUser({
        email: user.email,
        keyFingerprint: fingerprint
    });
    return res.json({
        //PRIVATE KEY IS CONSUMED!!
        privateKey: await Security.getPrivateKey(uuid)
    });
});

/*/!* GET home page. *!/
router.get('/', function (req, res, next) {
    if (req.session.userIsLoggedIn) {
        //already logged in stuff
        //show card with username, pub key, group name
        res.render('pages/userLogged', {
            title: 'User',
            activePage: 'user',
            username: req.session.user.username,
            groupName: req.session.user.groupName,
            publicKey: req.session.user.publicKey
        })
    } else {
        //login
        res.render('pages/userDef', {title: 'User', activePage: 'user'})
    }
});

router.post('/login', function (req, res, next) {
    if (req.session.userIsLoggedIn) {
        res.status(204).send();
        return true;
    }
    if (!req.body.username) {
        res.status(422).json({error: 'username not set!'});
        return false;
    }
    if (!req.body.password) {
        res.status(422).json({error: 'password not set!'});
        return false;
    }
    if (!validator.isAlphanumeric(req.body.username)) {
        res.status(422).json({error: 'username was not alphanumeric!'});
        return false;
    }
    const username = req.body.username;
    const password = req.body.password;

    return UserManager.getUserByUsername(username).then(user => {
        if (!user) {
            res.status(404).json({error: 'user not found!'});
            return false;
        }
        const hashedStored = user.password;
        const salt = user.salt;
        return Security.verifyPassword(password, hashedStored, salt).then(b => {
            if (b) {
                //password correct
                req.session.userIsLoggedIn = true;
                user.password = '';
                user.salt = '';
                req.session.user = user;
                res.status(204).send();
            } else {
                //password incorrect
                res.status(401).send();
                return false;
            }
        });
    });
});

router.post('/create', function (req, res, next) {
	res.status(404).send();
	return false;
    if (!req.body.username) {
        res.status(422).json({error: 'username not set!'});
        return false;
    }
    if (!req.body.password) {
        res.status(422).json({error: 'password not set!'});
        return false;
    }
    if (!req.body.groupName) {
        res.status(422).json({error: 'groupName not set!'});
        return false;
    }
    if (!req.body.key) {
        res.status(422).json({error: 'key not set!'});
        return false;
    }
    if (!validator.isAlphanumeric(req.body.username)) {
        res.status(422).json({error: 'username was not alphanumeric!'});
        return false;
    }
    if (!validator.matches(req.body.groupName.replaceAll(' ', ''))) {
        res.status(422).json({error: 'groupName was not alphanumeric!'});
        return false;
    }
    const key = req.body.key;
    const keyc = '3HXE!nv9aQgJ@BSU';
    if (key !== keyc) {
        res.status(403).json({error: 'incorrect key!'});
        return false;
    }
    const username = req.body.username;
    const password = req.body.password;
    const groupName = req.body.groupName;
    let output = {};

    return UserManager.getUserByUsername(username).then(user => {
        if (user) {
            //user already created
            res.status(409).json({error: `user ${username} already exists!`});
            return false;
        } else {
            const user = User();
            user.username = username;
            user.groupName = groupName;
            const encryptedPasswordDataP = Security.encryptPassword(password);
            const keyPs = Security.generateKeypair(username, password).then(b => {
                if (b) {
                    //keypair generation success
                    const privateKeyP = Security.getPrivateKey(username);
                    const publicKeyP = Security.getPublicKey(username);
                    return Promise.all([privateKeyP, publicKeyP]);
                } else {
                    res.status(500).json({error: `could not generate keypair for ${username}!`});
                    return false;
                }
            });
            const keyFingerprintP = keyPs.then(keys => {
                const privKey = keys[0];
                const pubKey = keys[1];
                user.publicKey = pubKey;
                output.privKey = privKey;
                output.pubKey = pubKey;

                return DigitalOcean.addPublicKey(username, pubKey);
            });
            const final = Promise.all([encryptedPasswordDataP, keyFingerprintP]).then(data => {
                const encryptedPasswordData = data[0];
                const keyFingerprint = data[1];
                user.salt = encryptedPasswordData.salt;
                user.password = encryptedPasswordData.passwordHash;
                user.keyFingerprint = keyFingerprint;
                return UserManager.updateUser(user);
            });
            return final.then(b => {
                res.status(201).json(output);
                return true;
            })
        }
    });
});

router.delete('/', function (req, res, next) {
    if (!req.session.userIsLoggedIn) {
        res.status(401).json({error: 'login required.'});
    }
    if (!req.body.username) {
        res.status(422).json({error: 'username not set!'});
        return false;
    }
    if (!validator.isAlphanumeric(req.body.username)) {
        res.status(422).json({error: 'username was not alphanumeric!'});
        return false;
    }
    const username = req.body.username;
    if (!username || req.session.user.username !== username) {
        res.status(401).json({error: 'login required.'});
        return false;
    }
    //lookup user
    return UserManager.getUserByUsername(username).then(user => {
        if (!user) {
            res.status(404).json({error: 'user not found!'});
            return false;
        }
        const keyDeleteP = DigitalOcean.deletePublicKey(user.keyFingerprint);
        const deleteUserP = UserManager.dropUser(user);
        return Promise.all([keyDeleteP, deleteUserP]).then(data => {
            const keyDeleted = data[0];
            const userDeleted = data[1].n === 1;
            if (!keyDeleted) {
                res.status(500).json({error: 'user key could not be deleted from Digial Ocean!'});
                return false;
            }
            if (!userDeleted) {
                res.status(500).json({error: 'user info could not be deleted from local store!'});
                return false;
            }
            res.status(204).send();
            return true;
        });
    });
});*/

module.exports = router;
