module.exports = {
    apps: [{
        name: 'SWEII-MC',
        script: 'bin/www',

        // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
        args: '',
        instances: 'max',
        exec_mode: 'cluster_mode',
        autorestart: true,
        watch: true,
        ignore_watch: [
            'node_modules',
            /\S*\.pub/
        ],
        max_memory_restart: '500M',
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production'
        }
    }]
};
