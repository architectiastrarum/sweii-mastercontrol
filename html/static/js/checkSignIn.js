const checkAuth = async () => {
    const authCheckRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/auth/check',
        type: 'GET',
        contentType: 'application/json; charset=utf-8'
    });
    return (await authCheckRequest).authenticated;
};
const updateSignInButton = async () => {
    const signInButton = $('#signInButton');
    const signOutButton = $('#signOutButton');

    const authenticated = await checkAuth();

    if (authenticated) {
        signInButton.hide();
        signOutButton.show();
    }
};
const signInRedirect = async () => {
    if (!window.location.pathname.includes('index') && window.location.pathname !== '/') {
        //not the index page
        if (!(await checkAuth())) {
            //not signed in
            window.location.replace('./api/auth/google');
        }
    }
};
document.addEventListener('DOMContentLoaded', async () => {
    await signInRedirect();
    await updateSignInButton();
});