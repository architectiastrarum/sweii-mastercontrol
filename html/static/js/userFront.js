const updateUserStatus = async () => {
    const statusDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/user',
        type: 'GET',
        contentType: 'application/json; charset=utf-8'
    });
    const keypairDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/user/keypair',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {
            generate: false
        }
    });
    statusDeferredRequest.catch((err) => {
        console.log(err);
    });
    keypairDeferredRequest.catch((err) => {
        console.log(err);
    });

    const userEmailField = $('#userEmail');
    const userPublicKey = $('#userPublicKey');
    const userKeyFingerprint = $('#userKeyFingerprint');
    const userDroplets = $('#userDroplets');

    let user = {};
    let keypair = {};
    try {
        user = await statusDeferredRequest || {};
        keypair = await keypairDeferredRequest || {};
    } catch (e) {
        console.log(e)
    }

    const generateKeyHTML = `<form action="./api/user/keypair" method="post"><button type="button" class="btn btn-primary">Generate Keypair</button></form>`;

    userEmailField.fadeOut().html(user.email).fadeIn();
    if (!!keypair['publicKey']) {
        const publicKeyHTML = `<p class="bg-light text-monospace">${user.publicKey}</p>`;
        userPublicKey.fadeOut().html(publicKeyHTML).fadeIn();
        userKeyFingerprint.fadeOut().html(user.keyFingerprint).fadeIn();
    } else {
        userPublicKey.fadeOut().html(generateKeyHTML).fadeIn();
        userKeyFingerprint.fadeOut().html();
    }

    user.droplets.forEach((subDomain) => {
        const dropletListItem = `<li class="list-group-item">${subDomain}</li>`;
        userDroplets.append(dropletListItem);
    });

};
document.addEventListener("DOMContentLoaded", updateUserStatus);