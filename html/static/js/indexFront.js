const updateApiStatus = async () => {
    const statusDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api',
        type: 'GET',
        contentType: 'application/json; charset=utf-8'
    });
    const statusField = $('#apiStatus');
    const versionField = $('#apiVersion');
    const uptimeField = $('#apiUptime');
    statusDeferredRequest.catch((err) => {
        statusField.fadeOut().html('ERRORED').fadeIn();
        uptimeField.fadeOut().html('0:00:00').fadeIn();
    });
    const apiStatus = await statusDeferredRequest;
    //TODO: add error state
    statusField.fadeOut().html(apiStatus.status).fadeIn();
    versionField.fadeOut().html(apiStatus.version).fadeIn();
    uptimeField.fadeOut().html(apiStatus.uptime).fadeIn();
};
document.addEventListener('DOMContentLoaded', updateApiStatus);
setInterval(updateApiStatus, 10000);