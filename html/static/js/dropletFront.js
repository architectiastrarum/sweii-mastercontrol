const createDroplet = async (button) => {
    const createDropletTeam = $('#createDropletTeam').val();
    const createEnv = $('#createEnv').val();
    button.innerHTML = 'Creating Droplet...';
    const statusDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/droplet/createDroplet',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            team: createDropletTeam,
            env: createEnv
        })
    });
    const response = await statusDeferredRequest;
    if (response.statusCode === 202) {
        window.location.reload();
    }
};

const delDrop = async (dropletId) => {
    const deleteDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/droplet/',
        type: 'DELETE',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            dropletId: dropletId
        })
    });
    deleteDeferredRequest.catch((err) => {
        throw err;
    });
    return (await deleteDeferredRequest).statusCode === 202;
};

const dd = async (deleteButton) => {
    const form = deleteButton.parentNode;
    deleteButton.innerHTML = "Deleting...";
    deleteButton.disabled = true;
    form.elements.deleteDropletConfirm.disabled = true;
    $(form.elements.deleteDropletConfirm).fadeOut();
    const trow = $(form.parentNode.parentNode);
    if (form.deleteDropletConfirm.attributes['aria-pressed'].nodeValue === "true") {
        //confirmed, delete droplet
        trow.deleteInProgress = true;
        setTimeout(() => {
            redPulse(trow)
        }, 100);
        delDrop(form.elements.dropletId.value).then((deleted) => {
            trow.deleteInProgress = false;
            if (deleted) {
                trow.fadeOut();
            } else {
                deleteButton.innerHTML = "Deletion failed.";
            }
        });
    }

};

async function redPulse(trow, isRed = false) {
    if (trow.deleteInProgress) {
        if (!isRed) {
            trow.animate({
                "background-color": "#f5c6cb"
            }, 250, "easeInExpo", () => redPulse(trow, true));
        } else {
            trow.animate({
                "background-color": "#ffffff"
            }, 1000, "easeInExpo", () => redPulse(trow))
        }
    }
}

const toggleDelete = async (confirmButton) => {
    const form = confirmButton.parentNode;
    form.elements.deleteDroplet.disabled = !form.elements.deleteDroplet.disabled;
};

const updateCreateFQDN = () => {
    const team = $('#createDropletTeam').val();
    const env = $('#createEnv').val();
    const createFQDN = $('#createFQDN');
    createFQDN.val(`${team}.${env}.softwareengineeringii.com`);
};

const compIPV6 = (input) => {
    return input.replace(/\b(?:0+:){2,}/, ':');
};

const getDropletsForUser = async () => {
    const statusDeferredRequest = $.ajax({
        url: 'https://mc.softwareengineeringii.com/api/droplet/dropletsForUser',
        type: 'GET',
        contentType: 'application/json; charset=utf-8'
    });
    return await statusDeferredRequest;
};

const updateDropletsTable = async (droplets) => {
    const htmls = droplets.map((droplet) => {
        return `<tr>
                        <th scope="row">${droplet.subDomain}</th>
                        <td>${droplet.ipv4 || '0.0.0.0'}</td>
                        <td>${compIPV6(droplet.ipv6) || '::'}</td>
                        <td>${droplet.private || '0.0.0.0'}</td>
                        <td>Unknown</td>
                        <td>
                            <form id="testf" class="form-inline">
                                <input type="hidden" value="${droplet.dropletId}" name="dropletId">
                                <button type="button" class="btn btn-warning" data-toggle="button" aria-pressed="false" name="deleteDropletConfirm" title="Confirm Droplet Deletion" onclick="toggleDelete(this)">Arm</button>
                                <button type="submit" class="btn btn-danger" name="deleteDroplet" onclick="dd(this)" disabled>Detonate</button>
                            </form>
                        </td>
                    </tr>`;

    });
    const tbody = $('#droplets');
    tbody.empty();
    htmls.forEach((html) => {
        tbody.append(html);
    })
};

const updateDroplets = async () => {
    const droplets = await getDropletsForUser();
    await updateDropletsTable(droplets);
};
document.addEventListener('DOMContentLoaded', updateDroplets);
setInterval(updateDroplets, 10000);