const mongoose = require('mongoose');

const dropletSchema = require('./Droplet').schema;

const Schema = mongoose.Schema;

const userSchema = new Schema({
    googleId: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    publicKey: String,
    keyFingerprint: String,
    groupName: String,
    droplets: [dropletSchema]
});
module.exports = mongoose.model('User', userSchema);