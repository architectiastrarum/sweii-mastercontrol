const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const dropletSchema = new Schema({
    dropletId: {
        type: Number,
        unique: true
    },
    ipv4: String,
    ipv6: String,
    dnsIds: [Number],
    private: String,
    subDomain: String
});

module.exports = mongoose.model('Droplet', dropletSchema);