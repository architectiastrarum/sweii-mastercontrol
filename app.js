//--digest section--
const pjson = require('./package.json');
console.log(`${pjson.name} version ${pjson.version}.`);
console.log(process.env.NODE_ENV + " environment.");


//--library requires--
const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');
const helmet = require('helmet');
const mongoose = require('mongoose');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const fs = require('fs');

//routers require
const indexRouter = require('./routes/index');
const dropletRouter = require('./routes/droplet');
const userRouter = require('./routes/user');

//custom file requires
const Security = require('./security');
const User = require('./models/User');

//--database setup--
// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open.');
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.error('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected.');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination.');
        process.exit(0);
    });
});

mongoose.connect('mongodb://localhost/sweii-mc', {useNewUrlParser: true});
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

//--express setup--
const app = express();

app.set('trust proxy', true);

//request logging setup
/*app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
}));*/

//general setup
app.use(helmet());

//body parsing (included in express 4.6.0+)
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//cookie setup
app.use(cookieParser());
const store = new MongoDBStore({
    uri: 'mongodb://localhost/sweii-mc',
    collection: 'sessions'
});

// Catch errors for store init
store.on('error', function(error) {
    throw error;
});

app.use(session({
    name: 'SWEII-MC',
    secret: fs.readFileSync('./secret.hex', 'utf-8'),
    resave: true,
    saveUninitialized: true,
    store: store,
    proxy: true,
    cookie: {
        expires: 3 * 60 * 60 * 1000,
        secure: process.env.NODE_ENV === 'production',
        sameSite: 'lax',
        httpOnly: false
    }
}));

//passport setup
app.use(passport.initialize());
app.use(passport.session());

const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
const uri = process.env.NODE_ENV === 'dev' ? 'http://localhost:3000' : 'https://mc.softwareengineeringii.com';
passport.use(new GoogleStrategy({
        clientID: '469807481889-aef0gjft2fl20rchi7jk248ab2lfsagb.apps.googleusercontent.com',
        clientSecret: '_ByHu4V0cHGW6dBt58b-VRA5',
        callbackURL: `${uri}/api/auth/google/callback`
    },
    function (accessToken, refreshToken, profile, done) {
        const user = {
            googleId: profile.id
        };
        if (!profile.emails[0].value.includes('@stedwards.edu')) {
            return done(new Error('Only St. Edward\'s Accounts may be used to sign in.'));
        }
        User.findOneAndUpdate({$or: [{googleId: profile.id}, {email: profile.emails[0].value}]}, user, {
            upsert: true,
            new: true
        }, (err, doc) => {
            return done(err, doc);
        });
    }
));
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.get('/auth/google/', passport.authenticate('google', {
    scope: ['profile', 'email']
}));

app.get('/auth/google/callback/',
    passport.authenticate('google', {failureRedirect: '/'}),
    function (req, res) {
        res.redirect('/user');
    });
app.get('/auth/logout', function (req, res) {
    req.logout();
    res.status(205).send()
});

app.get('/auth/check', function (req, res) {
    return res.json({
        authenticated: !!req.user
    });
});

//catchall for all request to redirect to auth
// app.get('*', function (req, res, next) {
//     if (req.url === '/' || req.url === '/auth/google' || req.url === '/auth/google/callback') return next();
//     if (req.user) {
//         next();
//     } else {
//         res.redirect('/api/auth/google');
//     }
// });

//router uses
app.use('/', indexRouter);
app.use('/droplet', dropletRouter);
app.use('/user', userRouter);

//error handling setup
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//     next(createError(404));
// });
//
// // error handler
// app.use(function (err, req, res) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//     // render the error page
//     res.status(err.status || 500);
//     res.render('error');
// });

//--utilities--
//string replaceall utility
String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    if (search instanceof RegExp) {
        return target.replace(search, replacement);
    } else {
        return target.replace(new RegExp(search, 'g'), replacement);
    }
};

const u1 = {
    email: 'falsaffa@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO1Y9S6aS0Z781BGabKBSrTgZl3RRRbClSLahSANikbJ falsaffa-SWEII-MC',
    keyFingerprint: '0f:c7:74:0e:ed:d7:87:46:a0:70:5b:57:db:12:ff:96',
    groupName: 'team11'
};
const u2 = {
    email: 'rsummerl@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIORZnJkDK6flgaJfPXn3S2wzsAbh2/NnBKQNCjU9Sy+1 team23-SWEII-MC',
    keyFingerprint: '58:2a:17:8e:b2:e4:07:03:dc:19:1f:3a:b1:a4:94:19',
    groupName: 'team23'
};
const u3 = {
    email: 'rgarci13@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEo6ZnWucvnVhYDPrfuFKMdR2KaeN9L5/myzacMlhQgp andyG01-SWEII-MC',
    keyFingerprint: 'c1:c1:f6:e7:cb:e8:c9:b3:97:f6:2c:3d:42:39:31:24',
    groupName: 'team21'
};
const u4 = {
    email: 'nluna@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHqTjLD0IcmXxOxxDI+FlLJz66VrQ2j/sqSQNIA0n0kv Baconnator-SWEII-MC',
    keyFingerprint: '7b:c8:2e:5b:fb:7d:19:ac:4d:25:14:a9:9e:ae:39:bb',
    groupName: 'team14'
};
const u5 = {
    email: 'nscharol@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMpDeFWXEpNwsDpQ2Dl6FDndTv23RtACG73hLmFXwgFS nscharol-SWEII-MC',
    keyFingerprint: '34:d7:21:a4:41:c3:2e:76:d7:02:16:5a:71:bb:f7:58',
    groupName: 'team13'
};
const u6 = {
    email: 'mbenton@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILj7N2NIoShw6qoihNFPBgZK9iNEgsVckQyaKM49aieB mbenton117-SWEII-MC',
    keyFingerprint: 'b3:69:73:17:16:55:a8:ec:66:63:96:9a:4e:92:56:20',
    groupName: 'team22'
};
const u7 = {
    email: 'jyoung9@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHAjgU64VoudA/sEP9Ty4PtHrtwULViX2meDtfBoy1kc jyoung9-SWEII-MC',
    keyFingerprint: 'd7:75:68:ef:68:7a:d2:08:13:19:75:2b:8d:ec:c2:7b',
    groupName: 'team12'
};
const u8 = {
    email: 'tchallst@stedwards.edu',
    publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODfezdoH0uGIj/uNcCvUWbd9ktHv+BpTCYnvJzH9zMm RogueTabII',
    keyFingerprint: 'a4:56:70:9f:4c:7c:b2:b8:db:13:38:b0:d1:26:bf:38',
    groupName: 'teamXX'
};

// User.create(u1);
// User.create(u2);
// User.create(u3);
// User.create(u4);
// User.create(u5);
// User.create(u6);
// User.create(u7);
// User.create(u8);


module.exports = app;
