const got = require('got');
'use strict';
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const runTests = async function (host) {
    const shellCmds = [
        "nginx -t",
        "node -v",
        "npm -v",
        "certbot renew --dry-run",
        "mongo --eval 'db.runCommand({ connectionStatus: 1 })'"
    ];
    const httpEndpoints = [
        "/",
        "/api"
    ];

    const shellTestPs = shellCmds.map((cmd) => {
        return executeShellCommand(cmd, host);
    });

    const httpTests = httpEndpoints.map(async (endpoint) => {
        const out = {};
        const response = await got(`https://${host}${endpoint}`).catch((err) => {
            out[endpoint] = err;
            return out;
        });
        out[endpoint] = response.body || '';
        return out;
    });

    const shellTestsP = Promise.all(shellTestPs);
    const httpTestsP = Promise.all(httpTests);

    const shellTestStatusesP = shellTestsP.then((output) => {
        const data = output.reduce((subData, elem) => {
            const key = Object.keys(elem)[0];
            subData[key] = elem[key];
            return subData;
        }, {});
        const out = {};
        out.nginxTest = data['nginx -t'].stderr.includes('syntax is ok') && data['nginx -t'].stderr.includes('test is successful');
        out.nodeTest = data['node -v'].stderr === '';
        out.nmpTest = data['npm -v'].stderr === '';
        out.certbotTest = data['certbot renew --dry-run'].stdout.includes('Congratulations,');
        out.mongoTest = data["mongo --eval 'db.runCommand({ connectionStatus: 1 })'"].stderr === '';
        return out;

    });
    const httpTestStatusesP = httpTestsP.then((output) => {
        const data = output.reduce((subData, elem) => {
            const key = Object.keys(elem)[0];
            subData[key] = elem[key];
            return subData;
        }, {});
        const out = {};
        out.rootTest = !!data['/'] ? !data['/'].includes('"status"') && data['/'].includes('html') : false;
        out.apiTest = (() => {
            try {
                JSON.parse(data['/api']);
                return true;
            } catch (e) {
                return false;
            }
        })();
        return out;
    });
    const shellTestStatuses = await shellTestStatusesP;
    const httpTestStatuses = await httpTestStatusesP;
    return Object.assign(shellTestStatuses, httpTestStatuses, {"host" : host});
};

const executeShellCommand = async function (command, host) {
    const cmd = `ssh -4 -i all -o "StrictHostKeyChecking=no" root@${host} "${command}"`;
    const out = {};
    out[command] = await exec(cmd);
    return out;
};