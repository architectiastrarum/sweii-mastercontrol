const got = require('got');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const authorization = 'Bearer ' + 'fd33abe5631113b5eaf9b35dd6ac2e1246517c54afba4f43f90d11ea1d97e202';

class DigitalOcean {
    static async addPublicKey(uuid, key) {
        const request = await got('https://api.digitalocean.com/v2/account/keys', {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                name: uuid,
                public_key: key
            }
        });
        return request.body.ssh_key.fingerprint;
    }

    static async deletePublicKey(identifier) {
        const request = await got(`https://api.digitalocean.com/v2/account/keys/${identifier}`, {
            json: true,
            method: 'DELETE',
            headers: {
                Authorization: authorization
            }
        });
        return request.statusCode === 204;
    }

    static async createMachine(name, keyFingerprint) {
        name = name + '.softwareengineeringii.com';
        const request = await got('https://api.digitalocean.com/v2/droplets/', {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                name: name,
                region: "sfo2",
                size: "s-1vcpu-1gb",
                image: "ubuntu-18-04-x64",
                ssh_keys: [
                    keyFingerprint,
                    '5b:c6:5b:84:0a:55:c6:10:dc:22:41:e3:1d:16:1a:34', //John's private key
                    'a4:56:70:9f:4c:7c:b2:b8:db:13:38:b0:d1:26:bf:38' //TJ's private Key
                ],
                backups: false,
                ipv6: true,
                user_data: null,
                private_networking: true,
                volumes: null,
                tags: [
                    "sweii-auto"
                ]
            }
        }).catch(err => {
            throw err
        });
        return request.body.droplet.id;
    }

    static async deleteMachine(dropletId) {
        const request = await got(`https://api.digitalocean.com/v2/droplets/${dropletId}`, {
            json: true,
            method: 'DELETE',
            headers: {
                Authorization: authorization
            }
        }).catch(err => {
            throw err
        });
        // console.log(JSON.stringify(request.body.droplet.networks, null, 2));
        return request.statusCode === 204;
    }


    static async getMachine(dropletId) {
        const request = await got(`https://api.digitalocean.com/v2/droplets/${dropletId}`, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        }).catch(err => {
            throw err
        });
        // console.log(JSON.stringify(request.body.droplet.networks, null, 2));
        return request.body;
    }

    static async createDNSRecord(name, ipv4, ipv6) {
        const request1 = await got(`https://api.digitalocean.com/v2/domains/softwareengineeringii.com/records`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                type: 'A',
                name: name,
                data: ipv4,
                priority: null,
                port: null,
                ttl: 1800,
                weight: null,
                flags: null,
                tag: null
            }
        });
        const request2 = await got(`https://api.digitalocean.com/v2/domains/softwareengineeringii.com/records`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                type: 'AAAA',
                name: name,
                data: ipv6,
                priority: null,
                port: null,
                ttl: 1800,
                weight: null,
                flags: null,
                tag: null
            }
        }).catch(err => {
            throw err
        });
        if (request1.statusCode === 201 && request2.statusCode === 201) {
            return [request1.body.domain_record.id, request2.body.domain_record.id];
        }
        else {
            throw `Could not add DNS record for ${name}`;
        }
    }

    static async deleteDNSRecords(dnsIds) {
        const deleteDNSRecord = async (id) => {
            const request = await got(`https://api.digitalocean.com/v2/domains/softwareengineeringii.com/records/${id}`, {
                json: true,
                method: 'DELETE',
                headers: {
                    Authorization: authorization
                }
            }).catch(err => {
                throw err
            });
            return request.statusCode === 204
        };
        const delPs = dnsIds.map((id) => deleteDNSRecord(id));
        return (await delPs).every((b) => b);
    }
}

module.exports = DigitalOcean;